from utils import parser
from twilio.rest import Client

conf = parser.parse_config()

account = conf['twilio']['account']
token = conf['twilio']['token']
twilio_client = Client(account, token)

def get_twilio_client():
    return twilio_client
