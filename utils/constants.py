first_message = 'Hi <firstName>, I saw that your <productType> was delivered. How are you enjoying it so far?'
pos_message = 'Great, can you describe what you love most about <productType>?'
neg_message = 'I\'m sorry to hear that, what do you dislike about <productType>?'