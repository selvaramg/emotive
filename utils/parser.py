import yaml


def parse_config():
    dir = './../resources'
    config_file = 'config.yml'
    path = '{}/{}'.format(dir, config_file)
    f = open(path)
    config = yaml.load(f)
    return config


def get_azure_configs():
    conf = parse_config()
    azure_conf = conf['azure']
    url = azure_conf['url'] + "sentiment"
    api_token = azure_conf['key1']

    return url, api_token
