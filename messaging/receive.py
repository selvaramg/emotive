from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from nlp import sentiment
from store import db

app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_reply():
    from_number = request.form['From']
    body = request.form['Body']

    from_number = from_number.replace('+1', '')

    print('Recived {} from {}'.format(body, from_number))

    reply = ''

    senti = sentiment.get_sentiment(body)

    resp = MessagingResponse()

    id, name, ph, ty = db.get_prod_type_for(from_number)

    first, pos, neg = db.get_content()

    if senti is 'positive':
        reply = pos.replace('<firstName>', name).replace('<productType>', ty)
    else:
        reply = neg.replace('<firstName>', name).replace('<productType>', ty)


    print('Sending response ', reply)
    resp.message(reply)
    return str(resp)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5555)
