from factory import client_factory as cf

client = cf.get_twilio_client()

def send(text, to):
    number = '+12132961918'
    response = client.messages.create(to=to, from_=number, body=text)
    status = response._properties['status']
    print(status)

# send('OMG unix rocks man', '+12134087788')