from nlp import azure


def get_sentiment(text):
    score = azure.get_sentiment_score(text)
    print('Text and its sentiment score from Azure ', text, score)

    if score > 0.5:
        return 'positive'

    else:
        return 'negative'


def test():
    print(get_sentiment('Hello, I really liked the product. I use it all day'))
    print(get_sentiment('Your product sucks! Needs a lot of improvement'))
    print(get_sentiment('Yup it was okay'))


# test()