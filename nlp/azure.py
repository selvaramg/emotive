from utils import parser
import requests

url, api_token = parser.get_azure_configs()
headers = {"Ocp-Apim-Subscription-Key": api_token}


def get_sentiment_score(text):
    id = '1'
    documents = {'documents': [
        {'id': id, 'language': 'en',
         'text': text}
    ]}
    response = requests.post(url, headers=headers, json=documents)
    sentiments = response.json()

    if sentiments['errors']:
        print('Azure response error')
        return -1

    score = list(filter(lambda v: v['id'] == id, sentiments['documents']))[0]['score']
    return score


def test():
    print(get_sentiment_score('Yes I like it and I think I will buy again!'))


# test()