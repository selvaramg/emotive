import psycopg2
from utils import constants


def create_tables():
    commands = [
        "CREATE TABLE IF NOT EXISTS sms (id SERIAL PRIMARY KEY, name varchar(40), number varchar(15), product_type varchar(15))",
        "CREATE TABLE IF NOT EXISTS content (name varchar(10) PRIMARY KEY, message varchar(400))"
    ]
    conn = None
    try:
        conn = psycopg2.connect("dbname=selvaram user=postgres password=")
        cur = conn.cursor()
        for command in commands:
            cur.execute(command)
            print('Executed ', command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def insert_records():
    messages = {'first': constants.first_message, 'pos': constants.pos_message, 'neg': constants.neg_message}
    conn = None
    try:
        conn = psycopg2.connect("dbname=selvaram user=postgres password=")
        cur = conn.cursor()
        drop_cmd = 'TRUNCATE content'
        cur.execute(drop_cmd)
        print('Executed ', drop_cmd)
        for k, v in messages.items():
            v = v.replace("'", "")
            command = "INSERT INTO content values('{}', '{}')".format(k, v)
            cur.execute(command)
            print('Executed ', command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_tables()
    insert_records()