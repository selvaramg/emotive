import psycopg2


def add_entry(user, phone, prod_type):
    insert_cmd = "INSERT INTO sms (name, number, product_type) values ('{}', '{}', '{}')".format(user, phone, prod_type)
    conn = None
    try:
        conn = psycopg2.connect("dbname=selvaram user=postgres password=")
        cur = conn.cursor()
        cur.execute(insert_cmd)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    print('Executed ', insert_cmd)


def get_prod_type_for(number):
    command = "SELECT * FROM sms WHERE number='{}' order by id desc limit 1".format(number)
    conn = None
    id, name, ph, ty = '', '', '', ''
    try:
        conn = psycopg2.connect("dbname=selvaram user=postgres password=")
        cur = conn.cursor()
        cur.execute(command)
        id, name, ph, ty = list(cur)[0]
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    print('Executed ', command)

    return id, name, ph, ty


def get_content():
        command = 'SELECT * from content'
        conn = None
        first, pos, neg = '', '', ''
        try:
            conn = psycopg2.connect("dbname=selvaram user=postgres password=")
            cur = conn.cursor()
            cur.execute(command)
            for ty, text in list(cur):
                if ty == 'first':
                    first = text
                elif ty == 'pos':
                    pos = text
                else:
                    neg = text
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        print('Executed ', command)

        return first, pos, neg


def update_content(ty, new_text):
    new_text = new_text.replace("'", '')
    command = "UPDATE content SET message='{}' WHERE name='{}'".format(new_text, ty)
    conn = None
    try:
        conn = psycopg2.connect("dbname=selvaram user=postgres password=")
        cur = conn.cursor()
        cur.execute(command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    print('Executed ', command)


# print(update_content('first', 'pickachu'))
# print(get_content())