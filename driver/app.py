from flask import Flask, render_template, flash, request

from messaging import sms
from store import db

DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'

@app.route("/", methods=['GET'])
def welcome():
    first, pos, neg = db.get_content()
    messages = {'first': first, 'pos': pos, 'neg': neg}
    return render_template('index.html', mydict=messages)


@app.route("/submit", methods=['POST'])
def submit():
    data = dict(request.form)
    user = data['user'][0]
    ph = data['phone'][0]
    ty = data['type'][0]
    ph = ph.replace('+1', '')
    first, pos, neg = db.get_content()

    text_to_send = first.replace('<firstName>', user).replace('<productType>', ty)
    print('Sending {} to {}'.format(text_to_send, ph))
    sms.send(text=text_to_send, to=ph)
    db.add_entry(user, ph, ty)

    flash('Message successfully sent to ' + user)
    return render_template('confirmation.html')


@app.route("/changefirst", methods=['POST'])
def change_first():
    data = dict(request.form)
    comment_ = data['comment'][0]
    db.update_content('first', comment_)
    flash('Message successfully changed to ' + comment_)
    return render_template('confirmation.html')


@app.route("/changepos", methods=['POST'])
def change_pos():
    data = dict(request.form)
    comment_ = data['comment'][0]
    db.update_content('pos', comment_)
    flash('Message successfully sent to ' + comment_)
    return render_template('confirmation.html')


@app.route("/changeneg", methods=['POST'])
def change_neg():
    data = dict(request.form)
    comment_ = data['comment'][0]
    db.update_content('neg', comment_)
    flash('Message successfully sent to ' + comment_)
    return render_template('confirmation.html')


if __name__ == "__main__":
    app.run()